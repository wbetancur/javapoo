/* 1: Hacer un programa que ingrese en un vector de 
10 posiciones los múltiplos de 3, se debe imprimir el vector. */

public class EjercioUno {

    public static void main(String[] args) {
        int nros[];
        nros = new int[10];
        int cr = 3;
        for (int i = 0; i <= nros.length - 1; i++) {
            nros[i] = cr;
            cr = cr + 3;
        }

        // imprimir vector
        for (int j = 0; j <= nros.length - 1; j++) {
            System.out.print(nros[j] + " ");
        }
    }
}
