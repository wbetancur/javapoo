/* Programa que crea una matriz de 3*5, pide ingresar 
   los elementos,los imprime, suma */

import java.util.Scanner;

public class MatrizUno {

    private Scanner teclado;
    private int[][] mat;

    public void cargar() {
        teclado = new Scanner(System.in);
        mat = new int[3][5];
        for (int f = 0; f < 3; f++) {
            for (int c = 0; c < 5; c++) {
                System.out.print("Ingrese Elemento: ");
                mat[f][c] = teclado.nextInt();
            }
        }
        teclado.close();
    }

    public void imprimir() {
        for (int f = 0; f < 3; f++) {
            for (int c = 0; c < 5; c++) {
                System.out.print(mat[f][c] + " ");
            }
            System.out.println();
        }
    }

    public void sumarElementos() {
        int suma = 0;
        for (int f = 0; f < 3; f++) {
            for (int c = 0; c < 5; c++) {
                suma = suma + mat[f][c];
            }
        }
        System.out.println("La suma de todos los elemtos de la Matriz es: " + suma);
    }

    public static void main(String[] ar) {
        MatrizUno ma = new MatrizUno();
        ma.cargar();
        ma.imprimir();
        ma.sumarElementos();
    }

}