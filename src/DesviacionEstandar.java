/* 8: Promedio y Desviación Estándar: Desarrolla un programa que calcule el promedio de un vector de números.  */

import java.util.Scanner;

public class DesviacionEstandar {

    private Scanner teclado;
    private int[] vec = new int[10];
    private int sumatoria = 0;
    private float media = 0;
    private double varianza = 0.0;
    private double desviacion = 0.0;

    public void cargar() {
        teclado = new Scanner(System.in);
        vec = new int[10];
        System.out.println("Ingrese el valor de 10 numeros como muestra");
        for (int f = 0; f < 10; f++) {
            System.out.print("Ingrese Elemento: ");
            vec[f] = teclado.nextInt();
            sumatoria = sumatoria + vec[f];
        }
        teclado.close();
        media = sumatoria / 10; // media aritmetica
    }

    public void calcularVariables() {
        // se hace la suma de las diferencias respecto a la media
        for (int i = 0; i < 10; i++) {
            double rango;
            rango = Math.pow(vec[i] - media, 2f);
            varianza = varianza + rango;
        }
        varianza = varianza / 10f;
        // suma de diferencias sobre "n"
        // teniendo ya la varianza sólo debemos sacarle raiz cuadrada
        // tendremos la desviación estandar
        desviacion = Math.sqrt(varianza);
    }

    public static void main(String[] args) {

        // instanciar la clase
        DesviacionEstandar de = new DesviacionEstandar();
        de.cargar();
        de.calcularVariables();

        // resultados
        System.out.println("Media: " + de.media);
        System.out.println("Varianza: " + de.varianza);
        System.out.println("Desvianción Estándar: " + de.desviacion);
    }

}