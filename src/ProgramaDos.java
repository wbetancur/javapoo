import java.util.Scanner;

public class ProgramaDos {

    public static void main(String[] args) {
        int n1, n2;
        Scanner teclado = new Scanner(System.in);

        System.out.print("Ingrese un numero: ");
        n1 = teclado.nextInt();

        System.out.print("Ingrese otro Numero: ");
        n2 = teclado.nextInt();

        System.out.println("La suma de los dos numeros es: ");
        System.out.println(n1 + n2);

        teclado.close();
    }

}
