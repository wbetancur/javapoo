import java.util.Scanner;

public class InicioVector {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String lista[];
        lista = new String[5];

        for (int i = 0; i < lista.length; i++) {
            System.out.print("Ingrese nombre: ");
            lista[i] = scanner.next();
        }

        // imprimir
        for (int i = lista.length - 1; i >= 0; i--) {
            System.out.println(lista[i]);
        }

        scanner.close();
    }
}
