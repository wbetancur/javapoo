/*  
programa que ingresa un numero por pantalla 
y lo imprime
*/

import java.util.Scanner;

public class IngresarNumero {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int numero;
        System.out.print("Ingrese numero: ");
        numero = teclado.nextInt();
        System.out.println("el numero ingresado es: " + numero +
                " java es muy facil");
        teclado.close();

    }
}
