import java.util.Scanner;

public class ListaInversa {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String lista[];
        lista = new String[5];

        // ingresar nombres al arreglo
        for (int i = 0; i < lista.length; i++) {
            System.out.print("Ingrese nombre: ");
            lista[i] = scanner.next();
        }

        // imprimir arreglo inverso
        System.out.print("Lista = [ ");
        for (int i = lista.length - 1; i >= 0; i--) {
            System.out.print(lista[i] + " ");
        }
        System.out.println(" ]");
        scanner.close();
    }
}
