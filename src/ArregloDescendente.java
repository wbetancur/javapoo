public class ArregloDescendente {

    public static void main(String[] args) {
        int vec[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int aux;

        for (int i = 0; i < vec.length - 1; i++) {
            for (int j = i + 1; j < vec.length; j++) {
                if (vec[j] > vec[i]) {
                    aux = vec[j];
                    vec[j] = vec[i];
                    vec[i] = aux;
                }
            }
        }

        for (int i = 0; i < vec.length - 1; i++) {
            System.out.println(vec[i]);
        }

    }

}
