// clase que una está en construccion

import java.util.Scanner;

public class Persona {

    private String nombre;
    private String identificacion;
    private String email;

    public String getNombre() {
        return nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Persona(String nombre, String identificacion, String email) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.email = email;
    }

    public void CargarDatos() {
        Scanner scanner = new Scanner(System.in);
        Persona estudiante = new Persona(nombre, identificacion, email);
        System.out.print("Ingrese Nombre:");
        estudiante.nombre = scanner.next();
        System.out.print("Ingrese identificacion: ");
        estudiante.identificacion = scanner.next();
        System.out.print("Ingrese email: ");
        estudiante.email = scanner.next();
        scanner.close();
        // return estudiante;
    }

    public static void main(String[] args) {
        Persona persona1 = new Persona(null, null, null);
        persona1.CargarDatos();
        System.out.println(persona1.nombre + " " +
                persona1.identificacion + " " +
                persona1.email);
    }

}
