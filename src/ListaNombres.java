import java.util.Scanner;

public class ListaNombres {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String lista[];
        lista = new String[5];
        for (int i = 0; i < lista.length; i++) {
            System.out.print("Ingrese nombre: ");
            lista[i] = scanner.next();
            // borra pantalla es linux
            System.out.print("033[H\033[2J");
            // borra pantalla de guindows
            // Runtime.getRuntime().exec("cls");
        }
        scanner.close();
    }
}
