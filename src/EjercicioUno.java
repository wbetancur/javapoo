import java.util.Scanner;

public class EjercicioUno {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n;

        System.out.printf("Ingrese un numero: ");
        n = scanner.nextInt();

        System.out.println("el numero Ingresado es: " + n);

        scanner.close();
    }

}
