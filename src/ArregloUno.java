import java.util.Scanner;

public class ArregloUno {

    public static int[] LlenarArreglo() {
        Scanner scanner = new Scanner(System.in); 
        int nombre_array[];
        nombre_array = new int[10];
        for (int i = 0; i <= 9; i++) {
            System.out.print("Ingrese numero: ");
            nombre_array[i] =  scanner.nextInt();
        }
        scanner.close();
        return nombre_array;
    }

    public static void ImprimirArreglo(int[] arreglo) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i <= 9; i++) {
            System.out.println(arreglo[i]);
        }
        scanner.close();
    }

    public static int SumarArreglo(int[] arreglo) {
        int suma = 0;
        for(int i = 0; i < arreglo.length; i++) {
            suma = suma + arreglo[i];
        }
        return suma;
    }

    public static int[] OdenarArreglo(int[] arreglo){
      int aux;
     // System.out.println(arreglo.length);
      for (int i = 0; i < arreglo.length - 1; i++) {
        for (int j = i + 1; j < arreglo.length; j++) {
            if (arreglo[j] < arreglo[i]) {
              aux = arreglo[j];
              arreglo[j] = arreglo[i];
              arreglo[i] = aux;
            }
        }
      }
      return arreglo;
    }

    public static void main(String[] args) {
        int nombre_array[]; 
        // llenar el arreglo 
        nombre_array = LlenarArreglo();  
        // imprimir todos los elementos del arreglo
        ImprimirArreglo(nombre_array); 
        // Imprimir la suma de todos los elementos del arreglo
        System.out.println("La suma de todos los elementos del arreglo es: " +  SumarArreglo(nombre_array));
        // ordenar arreglo
        ImprimirArreglo(OdenarArreglo(nombre_array));
    }
}
